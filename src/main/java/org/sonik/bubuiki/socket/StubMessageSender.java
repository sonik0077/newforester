package org.sonik.bubuiki.socket;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import javax.websocket.Session;

/**
 * Created by sonik on 25.08.2015.
 */

@AllArgsConstructor
public class StubMessageSender implements Runnable {

    private Session session;

    @SneakyThrows
    public void run() {
        int i = 300;
        while (i > 0) {
            session.getBasicRemote().sendText("Hi babe!");
            Thread.sleep(2000);
            i--;
        }
    }
}
