package org.sonik.bubuiki.socket;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonik on 25.08.2015.
 */

@ServerEndpoint("/listener")
public class GameSocket {

    private Map<String, Thread> sessionMap = new HashMap<String, Thread>();

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket opened: " + session.getId());
    }

    @OnMessage
    public void onMessage(String txt, Session session) throws IOException {
        System.out.println("Message received: " + txt);
        if (sessionMap.get(session.getId()) == null){
            Thread thread = new Thread(new StubMessageSender(session));
            thread.setDaemon(true);
            thread.start();
            sessionMap.put(session.getId(), thread);
        }
    }

    @OnClose
    public void onClose(CloseReason reason, Session session) {
        System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
    }

}
