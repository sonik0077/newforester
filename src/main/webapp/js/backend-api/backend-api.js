/**
 * Created by sonik on 25.08.2015.
 */
define('BackendService' , [],function(){
    function BackendService(serverUrl, onMessageCallback) {

        this.serverUrl = serverUrl;

        var self = {
            socket: null,
            onMessageCallback: onMessageCallback
        }

        this.connectToServer = function(onOpenCallback) {
            self.socket = new WebSocket(serverUrl);
            self.socket.onopen = function (event) {
                console.log("Successfully connected!");
                onOpenCallback();
            };
            self.socket.onmessage = function (event) {
                self.onMessageCallback(event.data);
            };
            self.socket.onclose = function (event) {
                console.log("Connection closed!");
            };
        }

        this.sendMessage = function(msg){
            self.socket.send(msg);
        }

        this.onMessageReceive = function(callback) {
            self.onMessageCallback = callback;
        }
    }
    return BackendService;
});
