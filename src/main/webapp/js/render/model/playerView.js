/**
 * Created by sonik on 26.08.2015.
 */
define('PlayerView', ['GlobalConfiguration', 'RenderHelper'], function(GlobalConfiguration, RenderHelper){
    function PlayerView(playerModel) {

        var self = {
            resources: {
                skinImg: null
            },
            textureDirection: 90
        };

        this.playerModel = playerModel;


        this.initialize = function () {
            self.loadResources();
        };

        this.render = function (context) {
            var height, width;
            height = context.canvas.clientHeight / GlobalConfiguration.gameFieldSize.height * GlobalConfiguration.playerSize.height;
            width = context.canvas.clientWidth / GlobalConfiguration.gameFieldSize.width * GlobalConfiguration.playerSize.width;
            var position = RenderHelper.getRealPosition(context, this.playerModel.position.x, this.playerModel.position.y,
                GlobalConfiguration.gameFieldSize.width, GlobalConfiguration.gameFieldSize.height);
            var directionAngle = this.playerModel.viewDirection - self.textureDirection;
            context.translate(position.x, position.y);
            context.rotate(2*Math.PI - RenderHelper.degreeToRad(directionAngle));
            context.drawImage(self.resources.skinImg, - width / 2, - height / 2, width, height);
            context.rotate(RenderHelper.degreeToRad(directionAngle));
            context.translate(-position.x, -position.y);
        }

        self.loadResources = function () {
            var img = new Image();
            img.src = GlobalConfiguration.playerSkinImg;
            self.resources.skinImg = img;
        }

        this.initialize();
    }

    return PlayerView;
});
