/**
 * Created by sonik on 26.08.2015.
 */
define('FieldView', ['PlayerView'], function(PlayerView){
    function FieldView(fieldModel) {
        this.fieldModel = fieldModel;
        this.playerViews = [];

        this.initialize = function() {
            for (var i = 0; i < this.fieldModel.players.length; i++) {
                this.playerViews.push(new PlayerView(this.fieldModel.players[i]));
            }
        }

        this.render = function(context) {
            for (var i = 0; i < this.playerViews.length; i++) {
                this.playerViews[i].render(context);
            }
        }

        this.initialize();
    }

    return FieldView;
});
