/**
 * Created by sonik on 25.08.2015.
 */
define('Canvas2DRenderer', [], function(){

    function Canvas2DRenderer(canvas) {
        this.canvas = canvas;

        this.continueRender = true;

        var self = {
            context: null,
            renderableObject: null,
            FPS: 60
        }

        this.setFPS = function(fps){
            self.FPS = fps;
        }

        this.setRenderableObject = function(object){
            self.renderableObject = object;
        }

        this.initialize = function (params) {
            self.context = this.canvas.getContext('2d');
            self.clear();
        }

        this.startRendering = function (params) {
            this.continueRender = true;
            setInterval(function () {
                if (!this.continueRender) {
                    self.render();
                }
            }, 1000 / self.FPS);
        }

        this.stopRendering = function (params) {
            this.continueRender = false;
        }

        self.clear = function () {
            self.context.fillRect(0, 0, canvas.width, canvas.height);
        }

        self.render = function () {
            self.clear();
            //console.log('Render! FPS: ' + self.renderableObject);
            if (self.renderableObject && self.renderableObject.render) {
                self.renderableObject.render(self.context);
            }
        }
    }

    return Canvas2DRenderer;

});

