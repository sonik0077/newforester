/**
 * Created by sonik on 26.08.2015.
 */
define('FieldModel', [], function(){
    function FieldModel(size, objects, players) {
        this.players = players;
        this.objects = objects;
        this.size = size;
    }

    return FieldModel;
});
