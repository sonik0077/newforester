/**
 * Created by sonik on 26.08.2015.
 */
define('PlayerModel', ['RenderHelper'], function(RenderHelper){
    function PlayerModel() {

        this.position = {
            x: 0.0,
            y: 0.0
        };
        this.health = 100;
        this.viewDirection = 0;
        this.name = 'unnamed';
        var self = {};
        this.move = function (step) {
            var dx, dy;
            dy = - Math.sin(RenderHelper.degreeToRad(this.viewDirection)) * step;
            dx = Math.cos(RenderHelper.degreeToRad(this.viewDirection)) * step;
            this.position.x += dx;
            this.position.y += dy;
            //console.log('dx = ' + this.position.x, 'dy = ' + this.position.y, 'angle = ' + this.viewDirection);
        }

        this.turn = function (angleDelta) {
            this.viewDirection = (angleDelta + this.viewDirection) % 360;
            this.viewDirection = this.viewDirection < 0 ? 360 + this.viewDirection : this.viewDirection;
        }
    }

    return PlayerModel;
});

