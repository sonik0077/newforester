/**
 * Created by sonik on 26.08.2015.
 */
define('RenderHelper', [], function(){
    function RenderHelper() {
    }

    RenderHelper.getRealPosition = function (context, x, y, width, height) {
        return {
            y: context.canvas.clientHeight / height * y,
            x: context.canvas.clientWidth / width * x
        }
    }

    RenderHelper.degreeToRad = function (degree) {
        return degree * Math.PI / 180;
    }

    return RenderHelper;
});

