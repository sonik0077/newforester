/**
 * Created by sonik on 25.08.2015.
 */
define('app', ['BackendService', 'PlayerController', 'PlayerModel', 'Canvas2DRenderer', 'FieldModel', 'FieldView'],
    function(BackendService, PlayerController, PlayerModel, Canvas2DRenderer, FieldModel, FieldView){

    function BbGame(userName, password) {

        var self = this;

        this.userName = userName;
        this.password = password;
        this.backendService = null;
        this.renderer = null;
        this.player = null;
        this.fieldModel = null;
        this.controller = null;
        this.element = null;


        this.initialize = function(){

            console.log("Game init started!");
            self.backendService = new BackendService('ws://localhost:8080/bubuiki/listener',function(msg){
                console.log(msg);
            });
            this.setCanvas(document.getElementById('main-canvas'));
            self.backendService.connectToServer(function(){
                document.getElementById('game-canvas').style.display = '';
                document.getElementById('start-button').style.display = '';
                document.getElementById('load-stage').style.display = 'none';
                document.getElementById('start-button').onclick = function(){
                    self.start();
                }
            });

        };

        this.start = function(){
            self.backendService.sendMessage('Hi beach');
            self.player = new PlayerModel();
            self.fieldModel = new FieldModel({},{},[this.player]);
            self.renderer.setRenderableObject(new FieldView(this.fieldModel));
            self.controller = new PlayerController(self.player, this.element);
        };

        this.setCanvas = function(canvas) {
            canvas.focus();
            canvas.onblur = function(){
                canvas.focus();
            };
            self.element = canvas;
            self.renderer = new Canvas2DRenderer(canvas);
            self.renderer.initialize({});
            self.renderer.startRendering({});
        };

    }
    var app = null;
    app = new BbGame('Sonik', '');
    return app;

});

