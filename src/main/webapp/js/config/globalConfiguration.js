/**
 * Created by sonik on 26.08.2015.
 */
define('GlobalConfiguration', [], function(){
    function GlobalConfiguration(){
        this.config = {
            playerSize: {
                width: 0.5,
                height: 0.5
            },
            scale: 1.0,
            gameFieldSize: {
                width: 10,
                height: 7
            },
            playerSkinImg: 'img/Android_logo.png',
            playerMoveSpeed: 3,
            playerRotateSpeed: 180,
            motionScale: 0.01,
            keys: {
                forward: 38,
                back: 40,
                left: 37,
                right: 39
            }
        }
    }
    var globalConfig = new GlobalConfiguration().config;
    return globalConfig;
})

