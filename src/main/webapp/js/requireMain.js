/**
 * Created by sonik on 16.09.2015.
 */
requirejs.config({
    baseUrl: 'js',
    paths: {
        'BackendService': 'backend-api/backend-api',
        'PlayerController': 'control/playerController',
        'Canvas2DRenderer': 'render/gl-renderer',
        'RenderHelper': 'util',
        'PlayerModel': 'model/playerModel',
        'FieldModel': 'model/fieldModel',
        'FieldView': 'render/model/fieldView',
        'GlobalConfiguration': 'config/globalConfiguration',
        'PlayerView':'render/model/playerView'
    }
});

requirejs([
    "app"
], function(app){
    app.initialize();
});