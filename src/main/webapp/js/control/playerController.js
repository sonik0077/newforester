/**
 * Created by sonik on 26.08.2015.
 */
define('PlayerController', ['GlobalConfiguration'], function(GlobalConfiguration){
    function PlayerController(playerModel) {

        var self = this;

        this.playerModel = playerModel;
        this.movingProcess = null;
        this.rotateProcess = null;
        this.rotating = false;
        this.moving = false;


        self.initialize = function () {
            window.addEventListener("keydown", function (event) {
                switch (event.keyCode) {
                    case GlobalConfiguration.keys.forward :
                        self.startMoving(true);
                        break;
                    case GlobalConfiguration.keys.back :
                        self.startMoving(false);
                        break;
                    case GlobalConfiguration.keys.left :
                        self.startRotate(true);
                        break;
                    case GlobalConfiguration.keys.right :
                        self.startRotate(false);
                        break;
                }
            }, true);
            window.addEventListener("keyup", function (event) {
                switch (event.keyCode) {
                    case GlobalConfiguration.keys.forward :
                        self.stopMoving();
                        break;
                    case GlobalConfiguration.keys.back :
                        self.stopMoving();
                        break;
                    case GlobalConfiguration.keys.left :
                        self.stopRotate();
                        break;
                    case GlobalConfiguration.keys.right :
                        self.stopRotate();
                        break;
                }
            }, true);
        }

        self.startRotate = function (left) {
            if (this.rotating) {
                return;
            } else {
                this.rotating = true;
            }
            var speed = GlobalConfiguration.playerRotateSpeed;
            var scale = GlobalConfiguration.motionScale;
            self.rotateProcess = setInterval(function () {
                self.playerModel.turn(left ? speed * scale : -speed * scale);
            }, 1000 * scale);
        };

        self.stopRotate = function () {
            this.rotating = false;
            clearInterval(self.rotateProcess);
        };

        self.startMoving = function (forward) {
            if (this.moving) {
                return;
            } else {
                this.moving = true;
            }
            var speed = GlobalConfiguration.playerMoveSpeed;
            var scale = GlobalConfiguration.motionScale;
            self.movingProcess = setInterval(function () {
                self.playerModel.move(forward ? speed * scale : -speed * scale);
            }, 1000 * scale);
        };

        self.stopMoving = function () {
            this.moving = false;
            clearInterval(self.movingProcess);
        };

        this.initialize();
    }

    return PlayerController;
});

